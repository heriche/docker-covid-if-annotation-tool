# Built with arch: amd64 flavor: lxde image: ubuntu:20.04
#
################################################################################
# base system
################################################################################

FROM ubuntu:20.04 as system



RUN sed -i 's#http://archive.ubuntu.com/ubuntu/#mirror://mirrors.ubuntu.com/mirrors.txt#' /etc/apt/sources.list;


# built-in packages
ENV DEBIAN_FRONTEND noninteractive
RUN apt update \
    && apt install -y --no-install-recommends software-properties-common curl apache2-utils git \
    && apt update \
    && apt install -y --no-install-recommends --allow-unauthenticated \
        supervisor nginx sudo net-tools zenity xz-utils \
        dbus-x11 x11-utils alsa-utils \
        mesa-utils libgl1-mesa-dri \
    && apt autoclean -y \
    && apt autoremove -y \
    && rm -rf /var/lib/apt/lists/*
# install debs error if combine together
RUN apt update \
    && apt install -y --no-install-recommends --allow-unauthenticated \
        xvfb x11vnc \
        vim-tiny firefox ttf-ubuntu-font-family ttf-wqy-zenhei  \
    && apt autoclean -y \
    && apt autoremove -y \
    && rm -rf /var/lib/apt/lists/*

RUN apt update \
    && apt install -y gpg-agent \
    && curl -LO https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
    && (dpkg -i ./google-chrome-stable_current_amd64.deb || apt-get install -fy) \
    && curl -sSL https://dl.google.com/linux/linux_signing_key.pub | apt-key add \
    && rm google-chrome-stable_current_amd64.deb \
    && rm -rf /var/lib/apt/lists/*

RUN apt update \
    && apt install -y --no-install-recommends --allow-unauthenticated \
        lxde gtk2-engines-murrine gnome-themes-standard gtk2-engines-pixbuf gtk2-engines-murrine arc-theme \
    && apt autoclean -y \
    && apt autoremove -y \
    && rm -rf /var/lib/apt/lists/*


# Additional packages require ~600MB
# libreoffice  pinta language-pack-zh-hant language-pack-gnome-zh-hant firefox-locale-zh-hant libreoffice-l10n-zh-tw

# tini to fix subreap
ARG TINI_VERSION=v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /bin/tini
RUN chmod +x /bin/tini

# python library
COPY rootfs/usr/local/lib/web/backend/requirements.txt /tmp/
RUN apt-get update \
    && dpkg-query -W -f='${Package}\n' > /tmp/a.txt \
    && apt-get install -y python3-pip python3-dev build-essential \
	&& pip3 install setuptools wheel && pip3 install -r /tmp/requirements.txt \
    && ln -s /usr/bin/python3 /usr/local/bin/python \
    && dpkg-query -W -f='${Package}\n' > /tmp/b.txt \
    && apt-get remove -y `diff --changed-group-format='%>' --unchanged-group-format='' /tmp/a.txt /tmp/b.txt | xargs` \
    && apt-get autoclean -y \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/cache/apt/* /tmp/a.txt /tmp/b.txt


################################################################################
# builder
################################################################################
FROM ubuntu:20.04 as builder


RUN sed -i 's#http://archive.ubuntu.com/ubuntu/#mirror://mirrors.ubuntu.com/mirrors.txt#' /etc/apt/sources.list;


RUN apt-get update \
    && apt-get install -y --no-install-recommends curl ca-certificates gnupg patch

# nodejs
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - \
    && apt-get install -y nodejs

# yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update \
    && apt-get install -y yarn

# build frontend
COPY web /src/web
RUN cd /src/web \
    && yarn \
    && yarn build
RUN sed -i 's#app/locale/#novnc/app/locale/#' /src/web/dist/static/novnc/app/ui.js



################################################################################
# merge
################################################################################
FROM system
LABEL maintainer="fcwu.tw@gmail.com"

COPY --from=builder /src/web/dist/ /usr/local/lib/web/frontend/
COPY rootfs /
RUN ln -sf /usr/local/lib/web/frontend/static/websockify /usr/local/lib/web/frontend/static/novnc/utils/websockify && \
	chmod +x /usr/local/lib/web/frontend/static/websockify/run

##### Add covid-if-annotations application ######

RUN git clone https://github.com/hci-unihd/covid-if-annotations.git

# Download miniconda3
SHELL [ "/bin/bash", "--login", "-c" ]
RUN wget http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O $HOME/miniconda3.sh && chmod 0755 $HOME/miniconda3.sh && $HOME/miniconda3.sh -b -p conda
ENV CONDA_PATH="/conda/bin"
RUN rm $HOME/miniconda3.sh
# RUN $CONDA_PATH/conda update conda
ENV PATH="$CONDA_PATH:$PATH"
RUN echo ". /conda/etc/profile.d/conda.sh" >> ~/.profile
RUN conda init bash

# Activate the covid-if-annotations environment
# and install the application
ARG CONDA_ENV=covid-if-annotations
RUN conda --version \
	&& conda create -c conda-forge -n ${CONDA_ENV} napari scikit-image h5py pandas \
	&& conda activate ${CONDA_ENV} && cd covid-if-annotations && pip install -e .

# Restore previous shell
SHELL ["/bin/sh", "-c"]
ENV PATH "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
##### End covid-if-annotations application #####

WORKDIR /root
# Make app start on boot
RUN echo '#!/bin/bash \n\
eval "$(conda shell.bash hook)" \n\
conda activate covid-if-annotations \n\
exec covid_if_annotations' > ~/covid-if-annotations.sh
RUN chmod +x ~/covid-if-annotations.sh
RUN mkdir -p ~/.config/lxsession/LXDE
RUN echo "bash /root/covid-if-annotations.sh" >> ~/.config/lxsession/LXDE/autostart

RUN mkdir -p ~/.local/share/applications
RUN mkdir -p ~/.config/autostart
RUN mkdir -p ~/Desktop
RUN echo "[Desktop Entry]\n\
Type=Application\n\
Path=/root\n\
Exec=lxterminal -e 'bash /root/covid-if-annotations.sh'" >> ~/.local/share/applications/CovidIfAnnotations.desktop
RUN cp ~/.local/share/applications/CovidIfAnnotations.desktop ~/.config/autostart/.
RUN cp ~/.local/share/applications/CovidIfAnnotations.desktop ~/Desktop/.
EXPOSE 80
ENV HOME=/home/ubuntu \
    SHELL=/bin/bash \
    RESOLUTION=2560x1440
HEALTHCHECK --interval=30s --timeout=5s CMD curl --fail http://127.0.0.1:6079/api/health

ENTRYPOINT ["/startup.sh"]
