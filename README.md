## Build a ubuntu-based vnc desktop for an image annotation tool
This is the napari-based tool used at EMBL to annotate an immunofluorescence assay measuring the antibody response of human serum to Sars-CoV-2.

Desktop from https://www.github.com/fcwu/docker-ubuntu-vnc-desktop

Tool available at: https://github.com/hci-unihd/covid-if-annotations

Build with: docker build --rm -t covid-if-annotations .

Run with: docker run -p 6079:80 covid-if-annotation-tool
